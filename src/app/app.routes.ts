import { RouterModule, Routes } from '@angular/router';
import { MatComponent } from '../components';

const routes: Routes = [
    { path: 'mat', component: MatComponent },    
    { path: '**', redirectTo: '' }
];

export const appRouterProviders = RouterModule.forRoot(routes, { useHash: true })