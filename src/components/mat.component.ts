import {Component} from '@angular/core';

@Component({
  selector: 'mat',
  templateUrl: './mat.component.html',
  styleUrls: ['./mat.component.css']
})
export class MatComponent {

  foods = [
    {value: 'steak-0', viewValue: 'MTC'},
    {value: 'pizza-1', viewValue: 'VELCOM'},
    {value: 'tacos-2', viewValue: 'LIFE'}
  ];

}

